﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OnSive.Common.IO
{
    public static class OSDirectory
    {
        /// <summary>
        /// Überprüft ob ein Verzeichnis existiert und erstellt dieses ggf.
        /// </summary>
        /// <param name="directoryInfo">Der Pfad zum Ordner</param>
        /// <param name="CreateFolder">Ob der Pfad nur überprüft werden soll, ohne diesen zu erstellen</param>
        /// <returns>Ob der Ordner existiert oder nicht</returns>
        public static bool Check(this DirectoryInfo directoryInfo, bool CreateFolder = true)
            => Check(directoryInfo.FullName, CreateFolder);

        /// <summary>
        /// Überprüft ob ein Verzeichnis existiert und erstellt dieses ggf.
        /// </summary>
        /// <param name="sPfad">Der Pfad zum Ordner</param>
        /// <param name="CreateFolder">Ob der Pfad nur überprüft werden soll, ohne diesen zu erstellen</param>
        /// <returns>Ob der Ordner existiert oder nicht</returns>
        public static bool Check(string sPfad, bool CreateFolder = true)
        {
            // Wenn der Ordner nicht existiert
            if (Directory.Exists(sPfad) == false)
            {
                // Wenn der Ordner nur überprüft werden soll, ohne diesen zu erstellen
                if (!CreateFolder)
                {
                    // Ordner existiert nicht
                    return false;
                }

                // Erstellt den Ordner
                Directory.CreateDirectory(sPfad);

                // Gibt zurück ob der Ordner erstellt wurde
                return Directory.Exists(sPfad);
            }

            // Ordner existiert
            return true;
        }

        /// <summary>
        /// Kopiert einen Ordner mit allen Dateien in Unter-Ordner
        /// </summary>
        public static void DeepCopy(this DirectoryInfo DirectoryInfo_Source, DirectoryInfo DirectoryInfo_Target)
        {
            // Für jeden Ordner
            foreach (DirectoryInfo DirectoryInfo_Tmp in DirectoryInfo_Source.GetDirectories())
            {
                // Recursiv aufrufen für den SubOrdner
                DeepCopy(DirectoryInfo_Tmp, DirectoryInfo_Target.CreateSubdirectory(DirectoryInfo_Tmp.Name));
            }

            // Für jede Datei
            foreach (FileInfo FileInfo_Tmp in DirectoryInfo_Source.GetFiles())
            {
                // Datei kopieren
                FileInfo_Tmp.CopyTo(Path.Combine(DirectoryInfo_Target.FullName, FileInfo_Tmp.Name));
            }
        }

        /// <summary>
        /// Gibt alle Dateien aus einem Ordner mit gewissen Extensions zurück
        /// </summary>
        /// <param name="DirectoryInfo_Tmp">Der Ordner, welcher durchsucht werden soll</param>
        /// <param name="enSearchOption">Die Such-Optionen</param>
        /// <param name="sExtensions">Die gesuchten Extensions (".exe", ".dll", ...)</param>
        /// <returns>Gibt eine Liste mit den gefundenen Dateien zurück</returns>
        public static IEnumerable<FileInfo> GetFiles(this DirectoryInfo DirectoryInfo_Tmp, SearchOption enSearchOption = SearchOption.AllDirectories, params string[] sExtensions) 
            => DirectoryInfo_Tmp.GetFiles("*", enSearchOption).Where(x => sExtensions.Contains(x.Extension, StringComparer.OrdinalIgnoreCase));
    }
}
