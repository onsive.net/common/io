﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace OnSive.Common.IO
{
    public static class OSFile
    {
        /// <summary>
        /// Dictionary für die verschiedenen ReaderWriterLocks für die Ordner
        /// </summary>
        private static readonly Dictionary<string, ReaderWriterLockSlim> Dictionary_LockDatei = new Dictionary<string, ReaderWriterLockSlim>();

        /// <summary>
        /// Überprüft ob eine Datei existiert und erstellt diese ggf.
        /// </summary>
        /// <param name="Path">Der Pfad zur Datei</param>
        /// <param name="CreateFile">Ob die Datei nur überprüft werden soll, ohne die Datei zu erstellen</param>
        /// <returns>Ob die Datei existiert oder nicht</returns>
        public static bool Check(this FileInfo fileInfo, bool CreateFile = true) => Check(fileInfo.FullName, CreateFile);

        /// <summary>
        /// Überprüft ob eine Datei existiert und erstellt diese ggf.
        /// </summary>
        /// <param name="Path">Der Pfad zur Datei</param>
        /// <param name="CreateFile">Ob die Datei nur überprüft werden soll, ohne die Datei zu erstellen</param>
        /// <returns>Ob die Datei existiert oder nicht</returns>
        public static bool Check(string Path, bool CreateFile = true)
        {
            // Wenn der string kein gültiger Pfad ist
            if (System.IO.Path.IsPathRooted(Path) == false)
            {
                // Ordner kann nicht existieren
                return false;
            }

            // Init FileInfo
            FileInfo oFileInfo = new FileInfo(Path);

            // Wenn der Parent Ordner nicht existiert
            if (OSDirectory.Check(oFileInfo.DirectoryName, CreateFolder: CreateFile) == false)
            {
                // Ordner existiert nicht / kann nicht erstellt werden
                return false;
            }

            // Wenn die Datei nicht existiert
            if (oFileInfo.Exists == false)
            {
                // Wenn die Datei nur überprüft werden soll, ohne diese zu generieren
                if (!CreateFile)
                {
                    // Datei existiert nicht
                    return false;
                }

                // Datei erstellen
                oFileInfo.Create().Close();

                // Gibt zurück ob die Datei erstellt wurde
                oFileInfo.Refresh();
                return oFileInfo.Exists;
            }

            // Datei existiert
            return true;
        }

        /// <summary>
        /// Ließt eine Datei ein und gibt den Inhalt zurück
        /// </summary>
        /// <param name="Path">Pfad zu der Datei</param>
        public static string Read(this FileInfo fileInfo) => Read(fileInfo.FullName);

        /// <summary>
        /// Ließt eine Datei ein und gibt den Inhalt zurück
        /// </summary>
        /// <param name="Path">Pfad zu der Datei</param>
        public static string Read(string Path)
        {
            try
            {
                // Sperren, sodass nicht durch einen anderen Thread die Liste zwischen des Checks und hinzufügen abändert
                lock (Dictionary_LockDatei)
                {
                    // Check ob der Schlüssel (Ordner Pfad) noch nicht existiert
                    if (Dictionary_LockDatei.ContainsKey(Path) == false)
                    {
                        // Fügt den Schlüssel hinzu (Ordner Pfad)
                        Dictionary_LockDatei.Add(Path, new ReaderWriterLockSlim());
                    }
                }

                // Sperrt den Ordner
                Dictionary_LockDatei[Path].EnterReadLock();

                // Wenn Datei nicht existiert
                if (!File.Exists(Path))
                {
                    // Fehler werfen
                    throw new FileNotFoundException("Eine Datei konnte nicht gefunden werden!", Path);
                }

                // Datei mit FileStream auslesen
                using (var FileStream_File = File.Open(Path, FileMode.Open, FileAccess.Read))
                {
                    // Datei in BufferStream übergeben (erhört die lese & schreib Geschwindigkeit)
                    using (var BufferedStream_File = new BufferedStream(FileStream_File))
                    {
                        // List den BufferedStream aus        
                        using (var StreamReader_File = new StreamReader(BufferedStream_File))
                        {
                            // Gibt den Inhalt zurück
                            return StreamReader_File.ReadToEnd();
                        }
                    }
                }
            }
            finally
            {
                // Entsperrt den Ordner wieder
                Dictionary_LockDatei[Path].ExitReadLock();
            }
        }

        /// <summary>
        /// Writes a string to a file
        /// </summary>
        /// <exception cref="FileNotFoundException">Occures when the passed file could not be found and created</exception>
        public static void Write(this FileInfo fileInfo, string sContent) => Write(fileInfo.FullName, sContent);

        /// <summary>
        /// Writes a string to a file
        /// </summary>
        /// <exception cref="FileNotFoundException">Occures when the passed file could not be found and created</exception>
        public static void Write(string Path, string Content)
        {
            try
            {
                // Sperren, sodass nicht durch einen anderen Thread die Liste zwischen des Checks und hinzufügen abändert
                lock (Dictionary_LockDatei)
                {
                    // Check ob der Schlüssel (Ordner Pfad) noch nicht existiert
                    if (Dictionary_LockDatei.ContainsKey(Path) == false)
                    {
                        // Fügt den Schlüssel hinzu (Ordner Pfad)
                        Dictionary_LockDatei.Add(Path, new ReaderWriterLockSlim());
                    }
                }

                // Sperrt den Ordner
                Dictionary_LockDatei[Path].EnterWriteLock();

                // Check ob der Pfad nicht existiert
                if (!OSFile.Check(Path))
                {
                    // Fehler werfen
                    throw new FileNotFoundException("A file could not be found!", Path);
                }

                // Leert die Datei
                File.WriteAllText(Path, string.Empty);

                // Datei mit FileStream auslesen
                using (FileStream FileStream_Datei = File.Open(Path, FileMode.Open, FileAccess.Write))
                {
                    // Datei in BufferStream übergeben (erhört die lese & schreib Geschwindigkeit)
                    using (BufferedStream BufferedStream_Datei = new BufferedStream(FileStream_Datei))
                    {
                        // List den BufferedStream aus        
                        using (StreamWriter StreamWriter_Datei = new StreamWriter(BufferedStream_Datei))
                        {
                            // Schreibt den Text in die Datei
                            StreamWriter_Datei.Write(Content);
                        }
                    }
                }
            }
            finally
            {
                // Entsperrt den Ordner wieder
                Dictionary_LockDatei[Path].ExitWriteLock();
            }
        }
    }
}
