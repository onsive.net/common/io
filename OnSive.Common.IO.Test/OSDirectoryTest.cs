﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OnSive.Common.IO.Test
{
    [TestClass]
    public class OSDirectoryTest
    {
        [TestMethod]
        public void Check()
        {
            DirectoryInfo di = new DirectoryInfo(Path.Combine(Environment.CurrentDirectory, "test"));

            if (di.Exists)
            {
                di.Delete(true);
            }

            Assert.IsFalse(OSDirectory.Check(di, false));
            Assert.IsFalse(di.Check(false));

            Assert.IsTrue(OSDirectory.Check(di));
            di.Delete(true);
            Assert.IsTrue(di.Check());
        }
    }
}
