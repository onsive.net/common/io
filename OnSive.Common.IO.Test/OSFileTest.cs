﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OnSive.Common.IO.Test
{
    [TestClass]
    public class OSFileTest
    {
        string sContent = "test123" + Environment.NewLine + "!\"§$%&/()=?`´´";

        [TestMethod]
        public void Check()
        {
            FileInfo fi = new FileInfo(Path.Combine(Environment.CurrentDirectory, "test", Guid.NewGuid().ToString()));

            if (fi.Exists)
            {
                fi.Delete();
            }

            Assert.IsFalse(OSFile.Check(fi, false));
            Assert.IsFalse(fi.Check(false));

            Assert.IsTrue(OSFile.Check(fi));
            fi.Delete();
            Assert.IsTrue(fi.Check());
        }

        [TestMethod]
        public void Read()
        {
            FileInfo fi = new FileInfo(Path.Combine(Environment.CurrentDirectory, Guid.NewGuid().ToString()));

            File.WriteAllText(fi.FullName, sContent);

            Assert.AreEqual(sContent, fi.Read());
            Assert.AreEqual(sContent, OSFile.Read(fi.FullName));
        }

        [TestMethod]
        public void Write()
        {
            FileInfo fi = new FileInfo(Path.Combine(Environment.CurrentDirectory, Guid.NewGuid().ToString()));

            fi.Write(sContent);
            OSFile.Write(fi + "#0", sContent);

            Assert.AreEqual(sContent, File.ReadAllText(fi.FullName));
            Assert.AreEqual(sContent, File.ReadAllText(fi.FullName + "#0"));
        }
    }
}
